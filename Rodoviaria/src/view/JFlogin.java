package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class JFlogin extends JFrame {

	private JPanel contentPane;
	private JTextField textUsuario;
	private JPasswordField textSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFlogin frame = new JFlogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFlogin() {
		setTitle("Sistema Rodovi\u00E1ria - Tela de Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSisRodoviariaBemVindo = new JLabel("SISRODOVI\u00C1RIA - BEM VINDO!");
		lblSisRodoviariaBemVindo.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 20));
		lblSisRodoviariaBemVindo.setBounds(10, 11, 326, 26);
		contentPane.add(lblSisRodoviariaBemVindo);
		
		JLabel lblInformeSuasCredenciais = new JLabel("Informe suas credenciais de acesso.");
		lblInformeSuasCredenciais.setFont(new Font("Dialog", Font.BOLD, 12));
		lblInformeSuasCredenciais.setBounds(10, 46, 216, 20);
		contentPane.add(lblInformeSuasCredenciais);
		
		JLabel lblUsuario = new JLabel("Usu\u00E1rio:");
		lblUsuario.setFont(new Font("Dialog", Font.BOLD, 12));
		lblUsuario.setBounds(10, 77, 75, 20);
		contentPane.add(lblUsuario);
		
		textUsuario = new JTextField();
		textUsuario.setBounds(70, 78, 354, 20);
		contentPane.add(textUsuario);
		textUsuario.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setFont(new Font("Dialog", Font.BOLD, 12));
		lblSenha.setBounds(10, 119, 46, 14);
		contentPane.add(lblSenha);
		
		textSenha = new JPasswordField();
		textSenha.setBounds(70, 117, 354, 20);
		contentPane.add(textSenha);
		
		JButton btnAcessar = new JButton("Acessar");
		btnAcessar.setBounds(10, 170, 89, 23);
		contentPane.add(btnAcessar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(109, 170, 89, 23);
		contentPane.add(btnCancelar);
		
		JButton btnCadastrarse = new JButton("Cadastrar-se");
		btnCadastrarse.setBounds(10, 215, 118, 23);
		contentPane.add(btnCadastrarse);
		
		JButton btnRecuperarSenha = new JButton("Recuperar Senha");
		btnRecuperarSenha.setBounds(138, 215, 135, 23);
		contentPane.add(btnRecuperarSenha);
	}
}
