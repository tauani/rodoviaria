package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.bean.passageiro;
import model.dao.passageiroDAO;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class JFListarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JTPassageiros;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarPassageiros frame = new JFListarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarPassageiros() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				readJTable();
			}
		});
		setTitle("Listar Passageiros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 743, 431);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblListarPassageiros = new JLabel("Listar Passageiros");
		lblListarPassageiros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblListarPassageiros.setBounds(10, 11, 197, 33);
		contentPane.add(lblListarPassageiros);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 55, 707, 218);
		contentPane.add(scrollPane);
		
		JTPassageiros = new JTable();
		JTPassageiros.setForeground(Color.GRAY);
		JTPassageiros.setFont(new Font("Tahoma", Font.PLAIN, 11));
		JTPassageiros.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
			},
			new String[] {
				"idPassageiro", "Nome", "G\u00EAnero", "RG", "CPF", "Endere\u00E7o", "E-mail", "Telefone"
			}
		));
		scrollPane.setViewportView(JTPassageiros);
		
		JButton btnCadastrarPassageiro = new JButton("Cadastrar Passageiro");
		btnCadastrarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFCadastrarPassageiro cp = new JFCadastrarPassageiro();
				cp.setVisible(true);
				
				
				
			}
		});
		btnCadastrarPassageiro.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCadastrarPassageiro.setBounds(10, 305, 151, 28);
		contentPane.add(btnCadastrarPassageiro);
		
		JButton btnAlterarPassageiro = new JButton("Alterar Passageiro");
		btnAlterarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JTPassageiros.getSelectedRow() != -1) {
					JFAtualizarPassageiro ap = new JFAtualizarPassageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(), 0));
					ap.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um Passageiro!");
				}
				readJTable();
			}
		});
		btnAlterarPassageiro.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAlterarPassageiro.setBounds(188, 305, 135, 28);
		contentPane.add(btnAlterarPassageiro);
		
		JButton btnExcluirPassageiro = new JButton("Excluir Passageiro");
		btnExcluirPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JTPassageiros.getSelectedRow() != -1) {
					int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o passageiro selecionado?", "Exclus�o", JOptionPane.YES_NO_OPTION);
					if (opcao == 0) {
						passageiroDAO dao = new passageiroDAO();
						passageiro p = new passageiro();
						p.setIdPassageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(), 0));
						dao.delete(p);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um passageiro!");
				}
				readJTable();
			}
		});
		btnExcluirPassageiro.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnExcluirPassageiro.setBounds(354, 305, 135, 28);
		contentPane.add(btnExcluirPassageiro);
		
		readJTable();
	}
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JTPassageiros.getModel();
		modelo.setNumRows(0);
		passageiroDAO pdao = new passageiroDAO();
		
		for(passageiro p : pdao.read()) {
			modelo.addRow(new Object[] {
					p.getIdPassageiro(),
					p.getNome(),
					p.getGenero(),
					p.getRg(),
					p.getCpf(),
					p.getEndereco(),
					p.getEmail(),
					p.getTelefone()
			});	
		}
	}
}
