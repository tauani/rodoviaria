package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.passageiro;
import model.dao.passageiroDAO;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFCadastrarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField textNome;
	private JTextField textRG;
	private JTextField textCPF;
	private JTextField textEndereco;
	private JTextField textEmail;
	private JTextField textTelefone;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadastrarPassageiro frame = new JFCadastrarPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastrarPassageiro() {
		setTitle("SISRODOVI\u00C1RIA - CADASTRAR PASSAGEIRO");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 559, 411);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCadastrar = new JLabel("CADASTRAR PASSAGEIRO");
		lblCadastrar.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCadastrar.setBounds(10, 11, 229, 20);
		contentPane.add(lblCadastrar);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNome.setBounds(10, 56, 46, 14);
		contentPane.add(lblNome);
		
		textNome = new JTextField();
		textNome.setBounds(50, 54, 481, 20);
		contentPane.add(textNome);
		textNome.setColumns(10);
		
		JLabel lblGenero = new JLabel("G\u00EAnero:");
		lblGenero.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblGenero.setBounds(10, 102, 46, 14);
		contentPane.add(lblGenero);
		
		JRadioButton rdbFeminino = new JRadioButton("Feminino");
		rdbFeminino.setBounds(67, 99, 95, 23);
		contentPane.add(rdbFeminino);
		
		JRadioButton rdbMasculino = new JRadioButton("Masculino");
		rdbMasculino.setBounds(169, 99, 95, 23);
		contentPane.add(rdbMasculino);
		
		JRadioButton rdbNaoBinario = new JRadioButton("N\u00E3o bin\u00E1rio");
		rdbNaoBinario.setBounds(266, 99, 104, 23);
		contentPane.add(rdbNaoBinario);
		
		JRadioButton rdbOutro = new JRadioButton("Outro");
		rdbOutro.setBounds(384, 99, 79, 23);
		contentPane.add(rdbOutro);
		
		ButtonGroup genero = new ButtonGroup();
		genero.add(rdbFeminino);
		genero.add(rdbMasculino);
		genero.add(rdbNaoBinario);
		genero.add(rdbOutro);
		
		
		JLabel lblRG = new JLabel("RG");
		lblRG.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblRG.setBounds(10, 144, 46, 14);
		contentPane.add(lblRG);
		
		JLabel lblCPF = new JLabel("CPF");
		lblCPF.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCPF.setBounds(266, 144, 46, 14);
		contentPane.add(lblCPF);
		
		textRG = new JTextField();
		textRG.setBounds(34, 142, 205, 20);
		contentPane.add(textRG);
		textRG.setColumns(10);
		
		textCPF = new JTextField();
		textCPF.setBounds(300, 142, 231, 20);
		contentPane.add(textCPF);
		textCPF.setColumns(10);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o");
		lblEndereco.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEndereco.setBounds(10, 188, 79, 14);
		contentPane.add(lblEndereco);
		
		textEndereco = new JTextField();
		textEndereco.setBounds(73, 186, 458, 20);
		contentPane.add(textEndereco);
		textEndereco.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEmail.setBounds(10, 233, 46, 14);
		contentPane.add(lblEmail);
		
		textEmail = new JTextField();
		textEmail.setBounds(50, 231, 481, 20);
		contentPane.add(textEmail);
		textEmail.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTelefone.setBounds(10, 278, 79, 14);
		contentPane.add(lblTelefone);
		
		textTelefone = new JTextField();
		textTelefone.setBounds(70, 276, 461, 20);
		contentPane.add(textTelefone);
		textTelefone.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				passageiro p = new passageiro();
				passageiroDAO dao = new passageiroDAO();
				
				p.setNome(textNome.getText());
				
				if (rdbFeminino.isSelected()) {
					p.setGenero("Feminino");
				} else if (rdbMasculino.isSelected()) {
					p.setGenero("Masculino");
				} else if (rdbNaoBinario.isSelected()) {
					p.setGenero("N�o Bin�rio");
				} else if (rdbOutro.isSelected()) {
					p.setGenero("Outro");
				}
				
				p.setRg(Long.parseLong(textRG.getText()));
				p.setCpf(textCPF.getText());
				p.setEndereco(textEndereco.getText());
				p.setEmail(textEmail.getText());
				p.setTelefone(Long.parseLong(textTelefone.getText()));
				
				dao.create(p);
				dispose();	
			}
		});
		
		btnCadastrar.setBounds(93, 338, 104, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textNome.setText(null);
				genero.clearSelection();
				textRG.setText(null);
				textCPF.setText(null);
				textEndereco.setText(null);
				textEmail.setText(null);
				textTelefone.setText(null);
			}
		});
		btnLimpar.setBounds(217, 338, 95, 23);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(333, 338, 104, 23);
		contentPane.add(btnCancelar);
	}
}
