package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.passageiro;

public class passageiroDAO {

	public void create(passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("INSERT INTO passageiro (nome, genero, rg, cpf, endereco, email, telefone) VALUES (?,?,?,?,?,?,?)");
			stmt.setString(1, p.getNome());
			stmt.setString(2, p.getGenero());
			stmt.setLong(3, p.getRg());
			stmt.setString(4, p.getCpf());
			stmt.setString(5, p.getEndereco());
			stmt.setString(6, p.getEmail());
			stmt.setLong(7, p.getTelefone());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public List<passageiro> read () {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<passageiro> passageiros = new ArrayList <>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				passageiro p = new passageiro();
		
				p.setIdPassageiro(rs.getInt("idPassageiro"));
				p.setNome(rs.getString("nome"));
				p.setGenero(rs.getString("genero"));
				p.setRg(rs.getLong("rg"));
				p.setCpf(rs.getString("cpf"));
				p.setEndereco(rs.getString("endereco"));
				p.setEmail(rs.getString("email"));
				p.setTelefone(rs.getLong("telefone"));
				
				passageiros.add(p);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informa��es do BD" + e);
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		
		return passageiros;
	}	
	
	public void delete(passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("DELETE FROM passageiro WHERE idPassageiro=?");
			stmt.setInt(1, p.getIdPassageiro());
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Filme exclu�do com sucesso!");
		} catch(SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir: " + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public passageiro read(int id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		passageiro p = new passageiro();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro WHERE idPassageiro=? LIMIT 1;");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			
			if (rs != null && rs.next()) {
				p.setIdPassageiro(rs.getInt("idPassageiro"));
				p.setNome(rs.getString("nome"));
				p.setGenero(rs.getString("genero"));
				p.setRg(rs.getLong("rg"));
				p.setCpf(rs.getString("cpf"));
				p.setEndereco(rs.getString("endereco"));
				p.setEmail(rs.getString("email"));
				p.setTelefone(rs.getLong("telefone"));
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		
		return p;
	}
	
	public void update(passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("UPDATE passageiro SET nome = ?, genero = ?, rg = ?, cpf = ?, endereco = ?, email = ?, telefone = ? WHERE idPassageiro = ?");
			stmt.setString(1, p.getNome());
			stmt.setString(2, p.getGenero());
			stmt.setLong(3, p.getRg());
			stmt.setString(4, p.getCpf());
			stmt.setString(5, p.getEndereco());
			stmt.setString(6, p.getEmail());
			stmt.setLong(7, p.getTelefone());
			stmt.setInt(8, p.getIdPassageiro());
			
			stmt.executeUpdate();
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
}