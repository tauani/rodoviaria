CREATE DATABASE rodoviaria;
USE rodoviaria;

CREATE TABLE Passageiro (
 idPassageiro INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 nome VARCHAR(100),
 genero VARCHAR(20),
 rg BIGINT,
 cpf VARCHAR(14),
 endereco VARCHAR(100),
 email VARCHAR(100),
 telefone BIGINT
);